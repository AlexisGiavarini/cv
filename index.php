<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="description" content="Version numérique de mon CV">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="stylesheet" href="css/style.css" type="text/css">

    <title>CV Alexis Giavarini</title>
</head>

<body>
   
    <!-- *********  main  ********** -->

    <main>

<!-- J'ai enlevé la face 1 et 6 car je ne m'en sers pas !-->
        <div id="experiment">
            <div id="cube">     
                <div id="formation" class="face formation attributForm">

                    <?php include 'header.html'; ?>
                    <?php include 'faceResponsive.html'; ?>
                    
                    <section class="texteCV" id="texte-formation">
                        <div id="formation-1">
                            <h4 class="date">2016-2019 La Rochelle,</h4>
                            <p class="faculte">Faculté des Sciences et Technologies </p>
                            <p class="titre">Licence informatique</p>

                            <p class="contenu"> Java, JavaScript, PHP, JQuery, Python, C Gestion de bases de données, Algorithmes et structures de données, Web. </p>
                        </div>
                        <div id="formation-2">
                            <h4 class="date">2011-2015 Poitiers,</h4>
                            <p class="faculte">U.F.R Lettres et langues </p>
                            <p class="titre">Licence Langues Etrangères Appliquées</p>

                            <p class="contenu">Anglais, Espagnol, Economie, Droit, Marketing, Communication, Commerce international, Gestion. </p>
                        </div>
                        <div id="formation-3">
                            <h4 class="date">2011 Cognac,</h4>
                            <p class="faculte">Lycée Jean Monnet </p>
                            <p class="titre">Baccalauréat Economie</p>
                        </div>
                    </section>      
        
                   <button id="" name="formation" class="bouton form formP attributForm faceForm formButton">Formation</button>
                   <button id="" name="experiences" class="bouton exp expP attributExp faceForm expButton">Experiences</button>
                   <button id="" name="competences" class="bouton comp compP attributComp faceForm compButton">Competences</button>
                   <button id="" name="interets" class="bouton inter interP attributInter faceForm interButton">Intêrets</button>

                   <?php include 'footer.html'; ?>
               
                </div>
                
                <div id="experiences" class="face experiences attributExp">

                   <?php include 'header.html'; ?>
                   <?php include 'faceResponsive.html'; ?>
                   
                   <section class="texteCV" id="texte-experience">
                        <div id="experience-1">
                            <h4 class="structure">Galaxy Hotels, Informatique</h4>
                            <h5 class="poste">Poste occupé : Développeur PHP-SQL (stage) </h5>
                            
                            <div class="missions-experiences">
                                <p>Missions ou tâches réalisées :</p>
                                <ul class="missions">
                                    <li>Gestion et mise à jour de l'outil GALAXY</li>
                                    <li>Optimisation</li>
                                    <li>Gestion d'une partie de la BDD</li>
                                    <li>Améliorations ergonomiques diverses</li>
                                </ul>
                            </div>
                        </div>
                        <div id="experience-2">
                            <h4 class="structure">Hotel du Soleil, Tourisme</h4>
                            <h5 class="poste">Poste occupé : Employé polyvalent saisonnier </h5>
                            
                              <div class="missions-experiences"> 
                                <p>Missions ou tâches réalisées :</p>
                                <ul class="missions">
                                    <li>Gestion des prix</li>
                                    <li>Reception physique et téléphonique</li>
                                    <li>Gestion logistique</li>
                                    <li>Service petits-déjeuners</li>
                                </ul>
                            </div>
                        </div>
                        <div id="experience-3">
                            <h4 class="structure">Hotel du Soleil, Tourisme</h4>
                            <h5 class="poste">Poste occupé : Employé polyvalent (stage) </h5>
                            
                            <div class="missions-experiences">
                                <p>Missions ou tâches réalisées :</p>
                                <ul class="missions">
                                    <li>Gestion des Réservations</li>
                                    <li>Reception, accueil, relationnel</li>
                                    <li>Suivi clientèle</li>
                                    <li>Entretien deslocaux</li>
                                </ul>
                            </div>
                        </div>
                    </section> 
                  
                   <button id="" name="formation" class="bouton form formP attributForm faceExp formaButton">Formation</button>
                   <button id="" name="experiences" class="bouton exp expP attributExp faceExp expButton">Experiences</button>
                   <button id="" name="competences" class="bouton comp compP attributComp faceExp compButton">Competences</button>
                   <button id="" name="interets" class="bouton inter interP attributInter faceExp interButton">Intêrets</button>
                   
                   <?php include 'footer.html'; ?>
                   
                </div>
                
                <div id="competences" class="face competences attributComp">
                    
                    <?php include 'header.html'; ?>
                    <?php include 'faceResponsive.html'; ?>
                    
                    <section class="texteCV" id="texte-competence">
                        <div id="competence-1">
                            <h4 class="domaine">Informatique</h4>                            
                                <p class="aspect">Principaux langages : HTML5, CSS3, Javascript (avec JQuery), PHP, Java, Python, C.</p>
                                <p class="aspect">Competences en algorithmie et gestion de base de données.</p>
                            </div>
                        <div id="competence-2">
                            <h4 class="domaine">Langues</h4>                            
                                <ul class="aspect">
                                    <li>Anglais  ★★★★☆</li>
                                    <li>Espagnol ★★★☆☆</li>
                                </ul>                       
                        </div>
                        <div id="competence-3">
                            <h4 class="domaine">Autres</h4>                            
                            <p class="aspect">Connaissances en Droit, Marketing, Economie, Commmerce internationnal.</p>

                        </div>
                        <div id="competence-4">
                            <h4 class="domaine">Exemple de créations</h4>
                               <p id="code-git">Les codes sources sont en libre accès sur mon Git (lien en bas).</p>                          
                                <ul class="aspect">
                                    <li>Création d'un site responsive (HTML,CSS,JS) dans le cadre d'un projet de L1.</li>
                                    <li>Création d'un site de gestion de thés, coté consommateur, avec gestion de la BDD.</li>
                                    <li>Création en cours d'une application alternative au site de thés, avec Kotlin sur Android Studio.</li>
                                    <li>Créations et projets divers accessibles sur mon Git.</li>
                                </ul>                            
                        </div>
                    </section>
                    
                   <button id="" name="formation" class="bouton form formP attributForm faceComp formButton">Formation</button>
                   <button id="" name="experiences" class="bouton exp expP attributExp faceComp expButton">Experiences</button>
                   <button id="" name="competences" class="bouton comp compP attributComp faceComp compButton">Competences</button>
                   <button id="" name="interets" class="bouton inter interP attributInter faceComp interButton">Intêrets</button>
                   
                   <?php include 'footer.html'; ?>
                   
                </div>
                
                <div id="interets" class="face interets attributInter">
                    
                    <?php include 'header.html'; ?>
                    <?php include 'faceResponsive.html'; ?>
                    
                    <section class="texteCV" id="texte-interet">
                        <div id="interet-1">
                            <h4 class="domaine">Informatique</h4>                            
                                <ul class="aspect">
                                    <li>Découverte et apprentissage de nouveaux langages et paradigmes.</li>
                                    <li>Créations personnelles : Sites dynamiques et responsives, applications, projets divers.</li>
                                    <li>Suivi de l'actualité autour de la programmation.</li>
                                </ul>
                            </div>
                        <div id="interet-2">
                            <h4 class="domaine">Sciences et culture</h4>                            
                                <ul class="aspect">
                                    <li>Lecture et visionnage de contenus de vulgarisateurs scientifiques.</li>
                                    <li>Lecture d'articles scientifiques.</li>
  
                                </ul>                            
                        </div>
                        <div id="interet-3">
                            <h4 class="domaine">Autres</h4>                            
                                <ul class="aspect">
                                    <li>Jeux vidéo.</li>
                                    <li>Livres.</li>
                                    <li>Films et series.</li>
                                </ul>
                        </div>
                    </section>
                    
                   <button id="" name="formation" class="bouton form formP attributForm faceInter formButton">Formation</button>
                   <button id="" name="experiences" class="bouton exp expP attributExp faceInter expButton">Experiences</button>
                   <button id="" name="competences" class="bouton comp compP attributComp faceInter compButton">Competences</button>
                   <button id="" name="interets" class="bouton inter interP attributInter faceInter interButton">Intêrets</button>
                   
                   <?php include 'footer.html'; ?>
                   
                </div>
                
                <div id="bas" class="face bas"></div>
            </div>
        </div>

    </main>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js" type="text/javascript"></script>
    <script src="js/canvas.js" type="text/javascript"></script>
</body>

</html>
