$(document).ready(rotation);

//Calcul de l'âge
const naissance = new Date(1993,04,22);
var age = Date.now() - naissance.getTime();
age = (Math.floor(age/31540000000) + ' ans'); //nombre de millisecondes dans une année
$((document).getElementsByClassName('age')).append(age);

var ouvert = false; //Savoir si on a fait apparaître un texte (boutons éloignés)

//*****Savoir sur quelle face on se trouve*****//
var formation = true; //par défaut on est sur celle ci
var experiences = false;
var competences = false;
var interets = false;

if($(window).width() < 1140){ //Si on commence en mode portable, on enleve les autres face
    $('#experiences,#competences,#interets').css("display", "none"); // On commence sur Formation
}

function rotation() {

    var xAngle = 0,
        yAngle = 0;

    document.addEventListener('click', function (e) {
        //Pour ne garder qu'un seule bouton repère
        if($(e.target).hasClass('bouton')){
            $((document).getElementsByClassName('repere')).removeClass('repere');
        }
        
        //switch sur les quatre boutons
        switch (true) { 
            case e.target.classList.contains('form'): // Si on appuie sur le bouton Formation
                //switch pour la face sur laquelle on se trouve
                switch (e.target.parentElement.id) {
                    case 'formation': // Et si on est sur la face "Formation"
                        break;        //On ne bouge pas (on ne fait rien)
                    case 'experiences'://Ou si on est sur la face "Experiences"
                        yAngle += 90;  //On modifie le Y du cube de façon à tomber sur la bonne face (avec webkit ligne 81)
                        break;
                    case 'competences': //Même logique
                        yAngle -= 180;
                        break;
                    case 'interets': //Même logique
                        yAngle -= 90;
                        break;
                }
                
                formation = true;
                experiences = false;
                competences = false;
                interets = false;
                
                $((document).getElementsByClassName('formButton')).toggleClass('repere'); //Sert de repère visuel pour la face courante
                
                break;

            case e.target.classList.contains('exp'):
                switch (e.target.parentElement.id) {
                    case 'formation':
                        yAngle -= 90;
                        break;
                    case 'competences':
                        yAngle += 90;
                        break;
                    case 'interets':
                        yAngle -= 180;
                        break;
                }
                
                formation = false;
                experiences = true;
                competences = false;
                interets = false;
                
                $((document).getElementsByClassName('expButton')).toggleClass('repere');
                
                break;

            case e.target.classList.contains('comp'):
                switch (e.target.parentElement.id) {
                    case 'formation':
                        yAngle -= 180;
                        break;
                    case 'experiences':
                        yAngle -= 90;
                        break;
                    case 'interets':
                        yAngle += 90;
                        break;
                }
                
                formation = false;
                experiences = false;
                competences = true;
                interets = false;
                
                $((document).getElementsByClassName('compButton')).toggleClass('repere');
                
                break;

            case e.target.classList.contains('inter'):
                switch (e.target.parentElement.id) {
                    case 'formation':
                        yAngle += 90;
                        break;
                    case 'experiences':
                        yAngle -= 180;
                        break;
                    case 'competences':
                        yAngle -= 90;
                        break;
                }
                
                formation = false;
                experiences = false;
                competences = false;
                interets = true;
                
                $((document).getElementsByClassName('interButton')).toggleClass('repere');
                
                break;
        }
        
        //Si on clique sur un bouton (sinon cliquer n'importe où marcherait)
        
        if($(e.target).hasClass('bouton')){
                
            //Si on change de face et que les boutons sont proches 
            if($(e.target.parentElement).hasClass('attributForm') && $(e.target).hasClass('attributForm') && ouvert == false){
                glisse();
            }
            else if($(e.target.parentElement).hasClass('attributExp') && $(e.target).hasClass('attributExp') && ouvert == false){
                glisse();
            }
            else if($(e.target.parentElement).hasClass('attributComp') && $(e.target).hasClass('attributComp') && ouvert == false){
                glisse();
            }
            else if($(e.target.parentElement).hasClass('attributInter') && $(e.target).hasClass('attributInter') && ouvert == false){
                glisse();
            }
            else{
                
                if($(e.target).hasClass('attributForm') && formation == true){
                    proche();                
                    setTimeout(glisse,1600);
                }
                if($(e.target).hasClass('attributExp') && experiences == true){
                    proche();
                    setTimeout(glisse,1600); //Approximatif,certes, mais pas bloquante
                }   
                if($(e.target).hasClass('attributComp') && competences == true){
                    proche();                
                    setTimeout(glisse,1600);
                }
                if($(e.target).hasClass('attributInter') && interets == true){
                    proche();                
                    setTimeout(glisse,1600);
                }
            }
                   
        }
        $('#cube').css('webkitTransform', "rotateX(" + xAngle + "deg) rotateY(" + yAngle + "deg)"), false //Fait tourner le cube avec les nouveaux X et Y

    });
};

$('#categories-responsive tr').click(function(){
    if($(this).siblings().hasClass('repere_responsive')){
        $(this).siblings().toggleClass('repere_responsive', false);
    }
    
    if(!$(this).hasClass('repere_responsive')){
        $(this).toggleClass('repere_responsive');
    } 
});

$('.formation-responsive-td').click(function(){  
    $('.formation-responsive').toggleClass('formation-responsive-visible', true);
    $('.formation-responsive').toggleClass('formation-responsive-invisible', false);
    
    $('.experiences-responsive').toggleClass('experiences-responsive-visible', false);
    $('.experiences-responsive').toggleClass('experiences-responsive-invisible', true);
    $('.competences-responsive').toggleClass('competences-responsive-visible', false);
    $('.competences-responsive').toggleClass('competences-responsive-invisible', true);
    $('.interets-responsive').toggleClass('interets-responsive-visible', false); 
    $('.interets-responsive').toggleClass('interets-responsive-invisible', true);
    $('.informations-responsive').toggleClass('informations-responsive-visible', false);
    $('.informations-responsive').toggleClass('informations-responsive-invisible', true);

});

$('.experiences-responsive-td').click(function(){
    $('.experiences-responsive').toggleClass('experiences-responsive-visible', true);
    $('.experiences-responsive').toggleClass('experiences-responsive-invisible', false);
    
    $('.formation-responsive').toggleClass('formation-responsive-visible', false);
    $('.formation-responsive').toggleClass('formation-responsive-invisible', true);
    $('.competences-responsive').toggleClass('competences-responsive-visible', false);
    $('.competences-responsive').toggleClass('competences-responsive-invisible', true);
    $('.interets-responsive').toggleClass('interets-responsive-visible', false); 
    $('.interets-responsive').toggleClass('interets-responsive-invisible', true);
    $('.informations-responsive').toggleClass('informations-responsive-visible', false);
    $('.informations-responsive').toggleClass('informations-responsive-invisible', true);
    
});

$('.competences-responsive-td').click(function(){
    $('.competences-responsive').toggleClass('competences-responsive-visible', true);
    $('.competences-responsive').toggleClass('competences-responsive-invisible', false);
    
    $('.formation-responsive').toggleClass('formation-responsive-visible', false);
    $('.formation-responsive').toggleClass('formation-responsive-invisible', true);
    $('.experiences-responsive').toggleClass('experiences-responsive-visible', false);
    $('.experiences-responsive').toggleClass('experiences-responsive-invisible', true);
    $('.interets-responsive').toggleClass('interets-responsive-visible', false); 
    $('.interets-responsive').toggleClass('interets-responsive-invisible', true);
    $('.informations-responsive').toggleClass('informations-responsive-visible', false);
    $('.informations-responsive').toggleClass('informations-responsive-invisible', true);
});

$('.interets-responsive-td').click(function(){
    $('.interets-responsive').toggleClass('interets-responsive-visible', true);
    $('.interets-responsive').toggleClass('interets-responsive-invisible', false);
    
    $('.formation-responsive').toggleClass('formation-responsive-visible', false);
    $('.formation-responsive').toggleClass('formation-responsive-invisible', true);
    $('.experiences-responsive').toggleClass('experiences-responsive-visible', false);
    $('.experiences-responsive').toggleClass('experiences-responsive-invisible', true);
    $('.competences-responsive').toggleClass('competences-responsive-visible', false);
    $('.competences-responsive').toggleClass('competences-responsive-invisible', true);
    $('.informations-responsive').toggleClass('informations-responsive-visible', false);
    $('.informations-responsive').toggleClass('informations-responsive-invisible', true);
});

$('.informations-responsive-td').click(function(){
    $('.informations-responsive').toggleClass('informations-responsive-visible', true);
    $('.informations-responsive').toggleClass('informations-responsive-invisible', false);
    
    $('.formation-responsive').toggleClass('formation-responsive-visible', false);
    $('.formation-responsive').toggleClass('formation-responsive-invisible', true);
    $('.experiences-responsive').toggleClass('experiences-responsive-visible', false);
    $('.experiences-responsive').toggleClass('experiences-responsive-invisible', true);
    $('.competences-responsive').toggleClass('competences-responsive-visible', false);
    $('.competences-responsive').toggleClass('competences-responsive-invisible', true);
    $('.interets-responsive').toggleClass('interets-responsive-visible', false);
    $('.interets-responsive').toggleClass('interets-responsive-invisible', true);

});


$(window).on('resize', function(){
    var win = $(this); //this = window
    if (win.width() >= 1125) {
        $('#formation,#experiences,#competences,#interets').css("display", "block");
        
        if($('.formation-responsive').hasClass('formation-responsive-visible')){
            $('.formation-responsive').toggleClass('formation-responsive-visible', false);
            $('.formation-responsive').toggleClass('formation-responsive-invisible', true);            
        }
        if($('.experiences-responsive').hasClass('experiences-responsive-visible')){
            $('.experiences-responsive').toggleClass('experiences-responsive-visible', false);
            $('.experiences-responsive').toggleClass('experiences-responsive-invisible', true);
        }
        if($('.competences-responsive').hasClass('competences-responsive-visible')){
            $('.competences-responsive').toggleClass('competences-responsive-visible', false);
            $('.competences-responsive').toggleClass('competences-responsive-invisible', true);
        }
        if($('.interets-responsive').hasClass('interets-responsive-visible')){
            $('.interets-responsive').toggleClass('interets-responsive-visible', false);
            $('.interets-responsive').toggleClass('interets-responsive-invisible', true);
        }
        if($('.informations-responsive').hasClass('informations-responsive-visible')){
            $('.informations-responsive').toggleClass('informations-responsive-visible', false);
            $('.informations-responsive').toggleClass('informations-responsive-invisible', true);
        }
        
        $(document.getElementsByClassName('repere_responsive')).toggleClass('repere_responsive', false);
        
    }
    if (win.width() < 1125) {
        if($('section').hasClass('texteCV_visible')){ //Si les textes sont apparants, on les fait disparaitre avant de passer responsive
            proche();
        }
        
        if(formation){
            $('#experiences,#competences,#interets').css("display", "none");
        }else if(experiences){
            $('#formation,#competences,#interets').css("display", "none");
        }else if(competences){
            $('#experiences,#formation,#interets').css("display", "none");
        }else if(interets){
            $('#experiences,#competences,#formation').css("display", "none");
        }
    }
});

function glisse() { //Ecarte les boutons et fait apparaître le texte
    $('#cube .form').toggleClass('formP', false);
    $('#cube .form').toggleClass('formG',true);

    $('#cube .exp').toggleClass('expP', false);
    $('#cube .exp').toggleClass('expG',true);

    $('#cube .comp').toggleClass('compP', false);
    $('#cube .comp').toggleClass('compG',true);

    $('#cube .inter').toggleClass('interP', false);
    $('#cube .inter').toggleClass('interG',true);
    
    $('.texteCV').toggleClass('texteCV_visible', true);
    $('.texteCV_invisible').toggleClass('texteCV_invisible', false);
    $('.texteCV').toggleClass('texteCV', false);

    ouvert = true;
}


function proche() { //rapproche les boutons et fait disparaître le texte    
    $('#cube .form').toggleClass('formG',false);
    $('#cube .form').toggleClass('formP',true);
    
    $('#cube .exp').toggleClass('expG', false);
    $('#cube .exp').toggleClass('expP',true);
   
    $('#cube .comp').toggleClass('compG', false);
    $('#cube .comp').toggleClass('compP',true);

    $('#cube .inter').toggleClass('interG', false);
    $('#cube .inter').toggleClass('interP',true);
    
    $('.texteCV_visible').toggleClass('texteCV_invisible', true);
    $('.texteCV_visible').toggleClass('texteCV_visible', false);
    $('.texteCV_invisible').toggleClass('texteCV', true);
    
    ouvert = false;
}