function canvasLogo() {
    
    var canvases = Array.from(document.getElementsByClassName('canvas')); //Pour plusieurs canvas il faut une classe
                                                                          //getElementsByClassName retourne un array qu'il faut parcourir pour dessiner chaque canvas
     
    for (let canvas of canvases) {
        
        if(canvas.getContext){ //Verification de la prise en charge
            var ctx = canvas.getContext('2d');
            //processeurs
            ctx.beginPath();
            ctx.fillStyle = 'rgba(255, 255, 130, 1)';

            for (var x = 464; x < 533; x+=4) {
                for (var y = 38; y < 107; y+=4) {
                    draw(x, y, 2, 2);
                }
            }

            // modify method to add to path instead
            function draw(x,y,width,height) {
              ctx.rect(x,y,width,height);
            }

            // when done, fill once
            ctx.fill();

            //circuits

            //Coté gauche
            ctx.beginPath();
            ctx.strokeStyle = 'rgba(255, 255, 130, 1)';
            ctx.lineJoin = 'round';
            ctx.lineWidth = 3;

            ctx.moveTo(465,47);
            ctx.lineTo(125,47);
            ctx.lineTo(105,15);
            ctx.lineTo(0,15);

            ctx.moveTo(465,57);
            ctx.lineTo(120,57);
            ctx.lineTo(100,25);
            ctx.lineTo(0,25);

            ctx.moveTo(465,67);
            ctx.lineTo(115,67);
            ctx.lineTo(95,35);
            ctx.lineTo(0,35);

            ctx.moveTo(465,77);
            ctx.lineTo(110,77);
            ctx.lineTo(90,45);
            ctx.lineTo(0,45);

            ctx.moveTo(465,87);
            ctx.lineTo(220,87);
            ctx.lineTo(200,117);
            ctx.lineTo(0,117);


            ctx.moveTo(465,97);
            ctx.lineTo(225,97);
            ctx.lineTo(205,127);
            ctx.lineTo(0,127);


            //Coté droit
            ctx.moveTo(533,47);
            ctx.lineTo(878,47);
            ctx.lineTo(898,15);
            ctx.lineTo(1000,15);

            ctx.moveTo(533,57);
            ctx.lineTo(883,57);
            ctx.lineTo(903,25);
            ctx.lineTo(1000,25);

            ctx.moveTo(533,67);
            ctx.lineTo(888,67);
            ctx.lineTo(908,35);
            ctx.lineTo(1000,35);

            ctx.moveTo(533,77);
            ctx.lineTo(893,77);
            ctx.lineTo(913,45);
            ctx.lineTo(1000,45);

            ctx.moveTo(533,87);
            ctx.lineTo(773,87);
            ctx.lineTo(793,117);
            ctx.lineTo(1000,117);

            ctx.moveTo(533,97);
            ctx.lineTo(768,97);
            ctx.lineTo(788,127);
            ctx.lineTo(1000,127);

            ctx.stroke();

        }
    }
}

document.addEventListener('DOMContentLoaded', canvasLogo, false);